package main

import (
	"fmt"
	"strconv"
)

//Board : Repersent a board
type Board [3][3]int

//Move : Repersent a space to move to
type Move int

//Player : Repersent a player, 1 = x and 2 = o
type Player int

func getBestMove(who Player, board Board) Move {
	maxScore := -99999
	var bestMove Move
	for _, move := range getAllPossibleMoves(board) {
		score := getMoveScore(who, board, move, 10)
		// fmt.Println(getAllPossibleMoves(board))
		if score > maxScore {
			maxScore = score
			bestMove = move
		}
		// fmt.Printf("ogplayer: %d, move: %d, score: %d \n", who, move, score)
	}
	return bestMove
}

func getAllPossibleMoves(board Board) (allPossibleMoves []Move) {
	for i := 0; i < 9; i++ {
		row, col := numberToRowCol(i)
		if board[row][col] == 0 {
			allPossibleMoves = append(allPossibleMoves, Move(i))
		}
	}
	return
}

func numberToRowCol(n int) (row, col int) {
	row = int(n / 3)
	col = n % 3
	return
}

func rowColToNumber(row, col int) int {
	return row*3 + col
}

func win(board Board, who Player) bool {
	for i := 0; i < 3; i++ {
		if (board[i][0] == board[i][1]) && (board[i][1] == board[i][2]) && (board[i][2] == int(who)) {
			return true
		}
		if (board[0][i] == board[1][i]) && (board[1][i] == board[2][i]) && (board[2][i] == int(who)) {
			return true
		}
	}
	if (board[0][0] == board[1][1]) && (board[1][1] == board[2][2]) && (board[2][2] == int(who)) {
		return true
	}
	if (board[0][2] == board[1][1]) && (board[1][1] == board[2][0]) && (board[2][0] == int(who)) {
		return true
	}
	return false
}

func lose(board Board, who Player) bool {
	// make who the other player
	return win(board, 3-who)
}

func over(board Board) bool {
	return win(board, 1) || lose(board, 1) || allFilled(board)
}

func allFilled(board Board) bool {
	return len(getAllPossibleMoves(board)) == 0
}

func whoWon(board Board) Player {
	if win(board, 1) {
		return 1
	} else if win(board, 2) {
		return 2
	} else if allFilled(board) {
		return 0
	}
	return -1
}

func playMove(board Board, who Player, move Move) Board {
	row, col := numberToRowCol(int(move))
	board[row][col] = int(who)
	return board
}

func getMoveScore(who Player, board Board, move Move, loseScore int) int {
	loseScore = max(loseScore, 0)
	boardIfMove := playMove(board, who, move)
	if win(boardIfMove, who) {
		return 10
	} else if lose(boardIfMove, who) {
		return loseScore
	} else if allFilled(boardIfMove) {
		return 0
	} else {
		score := 0
		for _, oppMove := range getAllPossibleMoves(boardIfMove) {
			boardIfOppMove := playMove(boardIfMove, 3-who, oppMove)
			if lose(boardIfOppMove, who) {
				score -= loseScore
			} else {
				for _, nextMove := range getAllPossibleMoves(boardIfOppMove) {
					score = min(getMoveScore(who, boardIfOppMove, nextMove, loseScore-1), score)
				}
			}
		}
		// oppMove := getBestMove(3-who, boardIfMove)
		// boardIfOppMove := playMove(boardIfMove, 3-who, oppMove)
		// if lose(boardIfOppMove, who) {
		// 	score = -20
		// } else {
		// 	for _, nextMove := range getAllPossibleMoves(boardIfOppMove) {
		// 		score = min(getMoveScore(who, boardIfOppMove, nextMove), score)
		// 	}
		// }
		return score
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func printBoard(board Board) {
	fmt.Println("_____________________________________")
	for _, row := range board {
		for _, c := range row {
			fmt.Print(c, " ")
		}
		fmt.Println()
	}
	fmt.Println("_____________________________________")
}

func boardFromString(boardString string) (Board, error) {
	var board [3][3]int
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			num, err := strconv.Atoi(string(boardString[i*3+j]))
			if err != nil {
				return board, err
			}
			board[i][j] = num
		}
	}
	return board, nil
}

func blankBoard() Board {
	var board [3][3]int
	return board
}
