package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	fmt.Println("Welcome!")
	fmt.Println("Input Seed! (Leave blank for all 0)")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	seed := scanner.Text()
	board := blankBoard()
	if len(seed) > 0 {
		board, _ = boardFromString(seed)
	}

	fmt.Println("Input computer player (1, 2)")
	scanner.Scan()
	computerPlayeri, _ := strconv.Atoi(scanner.Text())
	computerPlayer := Player(computerPlayeri)
	humanPlayer := 3 - computerPlayer

	fmt.Println("Input first player to move (1, 2)")
	scanner.Scan()
	firstPlayeri, _ := strconv.Atoi(scanner.Text())
	firstPlayer := Player(firstPlayeri)

	fmt.Println("Starting Board")
	printBoard(board)

	if over(board) {
		fmt.Println(whoWon(board), " won")
		return
	}

	if firstPlayer == computerPlayer {

		fmt.Println("Input computer first move (0-8) (leave blank for best)")
		scanner.Scan()
		firstMoves := scanner.Text()
		if len(firstMoves) < 1 {
			board = playMove(board, computerPlayer, getBestMove(computerPlayer, board))
		} else {
			firstMovei, _ := strconv.Atoi(firstMoves)
			firstMove := Move(firstMovei)
			board = playMove(board, computerPlayer, firstMove)
		}

		printBoard(board)
		if over(board) {
			fmt.Println(whoWon(board), " won")
			return
		}
	}

	for {

		fmt.Println("Input your move as ", humanPlayer)
		scanner.Scan()
		humanMove, _ := strconv.Atoi(scanner.Text())
		board = playMove(board, humanPlayer, Move(humanMove))
		printBoard(board)
		if over(board) {
			fmt.Println(whoWon(board), " won\n")
			return
		}
		computerMove := getBestMove(computerPlayer, board)
		fmt.Println("Computer move: ", computerMove)
		board = playMove(board, computerPlayer, computerMove)
		printBoard(board)
		if over(board) {
			fmt.Println(whoWon(board), " won")
			return
		}

	}

}
